# README #

Web server in linguaggio python

### Come usarlo? ###

Scaricare il progetto e avviare da linea di comando le istruzioni `python AngeloTinti_Server.py {porta}`
Porta sta ad indicare la porta sulla quale girerà l'applicazione, di default è impostata a 53000.

### Note ###
Il server è stato debuggato con la versione di python 3.7.4
Si potrebbero riscontrare dei problemi con versioni inferiori alla 3.0
