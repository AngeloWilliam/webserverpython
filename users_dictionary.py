from datetime import datetime
class UsersDictionary():
    def __init__(self,users):
        """
        Build object with a users dictionary

        Parameters
        ----------
        users : dictionary

        Returns
        -------
        None.

        """
        self.__users = users

    def emptyUsers(self):
        """
        Say if there are users inside the dictionary

        Returns
        -------
        True if there is no user

        """
        return not self.__users['accounts']

    def isUserPresent(self,userMail):
        """
        Say if a user is inside the dictionary
        
        Parameters
        ----------
        userMail : mail of user to seatch

        Returns
        -------
        True if his mail is in the dictionary

        """
        return not self.emptyUsers() and any(user['mail'] == userMail for user in self.__users['accounts'])
    
    def addUser(self,userMail,password):
        """
        Insert user with these userMail and password inside the dictionary

        Parameters
        ----------
        userMail : mail of user
        password : password of user

        Returns
        -------
        None.

        """
        newUser = {
            "mail":userMail,
            "password":password,
            "signup_time":datetime.now().strftime("%H:%M:%S %d/%m/%Y"),
        }
        self.__users['accounts'].append(newUser)

    def isCredentialsCorrect(self,userMail,password):
        """
        Say if there is a user with these userMail and password inside the disctionary

        Parameters
        ----------
        userMail : mail of user
        password : password of user

        Returns
        -------
        TYPE
            DESCRIPTION.

        """
        return not self.emptyUsers() and any(user['mail'] == userMail and user['password']==password for user in self.__users['accounts'])
    
    def getUsers(self):
        """
        Get dictionary of users
        
        Returns
        -------
        The users' dictionary

        """
        return self.__users
        