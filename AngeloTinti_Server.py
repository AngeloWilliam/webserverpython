#!/bin/env python
import sys, signal
import http.server
import socketserver
import cgi
import json
from http import cookies
import os
from hashlib import sha256
import threading
import time
from users_dictionary import UsersDictionary
from datetime import datetime
import create_page

USERS_JSON_FILE_NAME = 'users'
GET_REQUEST_JSON_FILE_NAME = 'get_request'
USER_FIELD_NAME = 'user'
USER_LASTINTERACT_FIELD_NAME = 'lastInteract'

def create_and_send_cookie(simple_server,logged,mail):
    """
    Redirect to homepage of website, set cookie of user and insert him in list of users logged.
    Parameters
    ----------
    simple_server : server object that is serving.
    logged : dictionary of user logged.
    mail : mail of user who is logging or signing.

    Returns
    -------
    None.

    """
    simple_server.send_response(303)
    simple_server.send_header('Location','/homepage.html')
    cookie = cookies.SimpleCookie()
    user_sha = sha256(str(time.time_ns()).encode('utf-8')).hexdigest()
    logged[user_sha] = {USER_FIELD_NAME:mail,USER_LASTINTERACT_FIELD_NAME:(round(time.time()/60))}
    cookie['user_sha'] = user_sha
    simple_server.send_header("Set-Cookie", cookie.output(header='', sep=''))

def update_last_interact(logged_user,hash):
    """
    Update last time of interact with actual time of user with cookie equal to hash.

    Parameters
    ----------
    logged_user : dictionaty of logged users
    hash : cookie code of user

    Returns
    -------
    None.

    """
    logged_user[hash][USER_LASTINTERACT_FIELD_NAME] = round(time.time()/60)


def write_request(filename,path,header):
    """
    Write on file the request submitted to server. The file name is used to understand request's type'
    Parameters
    ----------
    filename : file where register the request
    path : path of page requested
    header : header of HTTP request

    Returns
    -------
    None.

    """
    file_request = f'./data_json/{filename}.json'
    if os.stat(file_request).st_size == 0:
        initialize_json(filename)
    with open(file_request,'r+',encoding='utf-8') as file_:
        header_ = {}
        for head in header:
            header_[head] = header[head]
        request = {
            "HTTP_Path":path,
            "HTTP_Headers":header_,
            "Time":datetime.now().strftime("%H:%M:%S %d/%m/%Y"),
        }
        past_request = json.load(file_)
        past_request[filename].append(request)
        file_.seek(0)
        json.dump(past_request,file_,indent=4,ensure_ascii=False)


def initialize_json(filename_json):
    """
    Initialize json file with one field with name of file passed.
    Parameters
    ----------
    filename_json : file json to initialize

    Returns
    -------
    None.

    """
    with open(f'./data_json/{filename_json}.json','r+',encoding="utf-8") as file_json:
        data={}
        data[filename_json]=[]
        json.dump(data,file_json,indent=4,ensure_ascii=False)
        file_json.truncate()
        print(f'{filename_json}.json inizializzato')

def check_last_interact(logged_user,waiting):
    """
    Check last interact time of each user logged. If 30 minutes are passed do logout of user.
    Parameters
    ----------
    logged_user : dictionaty of logged users
    waiting : obect threading.Event() to syncronize thread

    Returns
    -------
    None.

    """
    while not waiting.is_set():
        print("Controllo se ci sono inattivi...")
        for user_sha in list(logged_user):
            # dopo 30 minuti di inattività si perde l'accesso al sito
            if round(time.time()/60) - logged_user[user_sha][USER_LASTINTERACT_FIELD_NAME] > 30:
                del logged_user[user_sha]
        waiting.wait(timeout=15)
    
def launch_thread_check_last_interact(logged_user):
    """
    Launch thread it checks last interact time of logged users
    Parameters
    ----------
    logged_user : dictionary of logged users

    Returns
    -------
    None.

    """
    check_thread = threading.Thread(target=check_last_interact,args=(logged_user,waiting_refresh))
    check_thread.daemon = True
    check_thread.start()


def signup_or_login(users_file,mail,psw,isLogin=False):
    """
    Do signup or login, choose with field isLogin. Users are read or written 
    in json file with name users_file

    Parameters
    ----------
    users_file : name of json file where are there users
    mail : mail of user is logging or signing
    psw : password of user is logging or signing
    isLogin : Bool to indicate if it's login or not. The default is False.

    Returns
    -------
    True if login or sign up end successfully, false otherwise
    """
    file_users = f'./data_json/{users_file}.json'
    areThereUsers = True
    psw = sha256(psw.encode('utf-8')).hexdigest()
    if os.stat(file_users).st_size == 0:
        initialize_json(users_file)
        areThereUsers = False
    if not isLogin or areThereUsers:
        with open(file_users,'r+',encoding="utf-8") as fileUsers:
            users = UsersDictionary(json.load(fileUsers))
            if users.isUserPresent(mail):
                return isLogin and users.isCredentialsCorrect(mail,psw)
            else:
                if not isLogin:
                    users.addUser(mail,psw)
                    fileUsers.seek(0)
                    json.dump(users.getUsers(),fileUsers,indent=4,ensure_ascii=False)
                return not isLogin

# Legge il numero della porta dalla riga di comando
if sys.argv[1:]:
    port = int(sys.argv[1])
else:
    port = 53000

#Dizionario degli utenti che hanno fatto il login
logged_in = {}

#Lista delle pagine per le quali non è necessario fare il login per accedere
principal_page = ['','index.html','sign_up.html']

#Indirizzo ip sul quale girerà il server
host = 'localhost'
site_name = f'http://{host}:{port}'

#Oggetto utilizzato per la sincronizzazione con il thread che controlla l'ora
#Degli ultimi accessi
waiting_refresh = threading.Event()

# Reset json request page
initialize_json("get_request")
initialize_json("post_request")

class ServerHandler(http.server.SimpleHTTPRequestHandler):
    def do_GET(self):
        write_request("get_request",f'{site_name}{self.path}',self.headers)
       
        # per motivi di leggibilità delle condizioni dell'if ho scritto il codice sottoforma di stringa
        get_cookie_value = 'cookies.SimpleCookie(self.headers.get("Cookie"))["user_sha"].value'
        
        #Controllo se il client ha dei cookie e se sono presenti nella lista degli utenti loggati
        if 'Cookie' in self.headers and eval(get_cookie_value) in logged_in:
            print("Controllo i tuoi cookie")
            # aggiorno il minuto dell'ultima iterazione fatta da questo utente
            update_last_interact(logged_in,eval(get_cookie_value))
            #se un utente loggato richiede una pagina di login o di registrazione 
            #lo rimando alla homepage del sito
            if self.path[1:] in principal_page:
                print("Sei già loggato ti tolgo dalle pagine di accesso")
                self.send_response(302)
                self.send_header('Location','/homepage.html')
            #Se ho richiesto la pagina di logout vengo reindirizzato all'index
            #e i dati realativi ai miei cookie vengono cancellati
            elif self.path[1:] == "logout.html":
                del(logged_in[eval(get_cookie_value)])
                self.send_response(302)
                self.send_header('Location','./')
        elif self.path[1:] not in principal_page:
            self.send_response(302)
            self.send_header('Location','./')
        
        http.server.SimpleHTTPRequestHandler.do_GET(self)

    def do_POST(self):
        form = cgi.FieldStorage(fp=self.rfile, headers=self.headers, environ={'REQUEST_METHOD':'POST'})
        formType = form.getvalue('type')
        mail = form.getvalue('mail')
        password = form.getvalue('psw')

        write_request("post_request",f'{site_name}{self.path}',self.headers)
        
        page = ''
        #In base alle tipologie di form inviate eseguo la richiesta. Se il login
        #o l'iscrizione non si concludono con successo restituisco una pagina scritta 
        #al momento che informa su cosa non è andato a buon fine
        if formType == "login":
            if not signup_or_login(USERS_JSON_FILE_NAME,mail,password,True):
                print("Credenziali login incorrette!")
                page = create_page.get_error_login()
                self.send_response(401)
            else:
                print("Login avvenuto con successo!")
                create_and_send_cookie(self,logged_in,mail)
        elif formType == "signup":
            if not signup_or_login(USERS_JSON_FILE_NAME,mail,password):
                print("Registrazione non avvenuta!")
                page = create_page.get_error_signup_yet()
                self.send_response(409)
            else:
                print("Registrazione avvenuta con successo!")
                create_and_send_cookie(self,logged_in,mail)
        
        #Se la pagina è stata costruita vuol dire che nel passo precedente qualcosa
        #non è andato
        if page:
            self.send_header("Content-type", "text/html")
            self.end_headers()
            self.wfile.write(bytes(page, 'utf-8'))
        else:
            #Se tutto è andato bene restituisco all'utente la homepage del sito
            self.do_GET()


print("host:", host,"\nPort:",port)
# Creo una socket TCP con i parametri precedentemente definiti, la informo inoltre
# che le richieste devono essere soddisfatte dal mio handler
server = socketserver.ThreadingTCPServer((host,port), ServerHandler)

# Faccio partire il thread che controllerà l'ora dell'ultima iterazione degli utenti loggati
launch_thread_check_last_interact(logged_in)

# Faccio partire il thread che aggiornerà le pagine del sito
create_page.launch_thread_refresh(waiting_refresh,host,port)

# Al termine del server faccio terminare tutti gli altri thread che ancora
# sono in esecuzione
server.daemon_threads = True

# il Server acconsente al riutilizzo del socket anche se ancora non è stato
# rilasciato quello precedente, andandolo a sovrascrivere
server.allow_reuse_address = False

# creo un handler per l'evento "pressione dei tasti CTRL+C"
# termino il server e anche gli altri thread
def signal_handler(signal, frame):
    print( 'Exiting http server (Ctrl+C pressed)')
    try:
        if( server ):
            server.server_close()
    finally:
        waiting_refresh.set()
        sys.exit(0)

# assegna un handler alla pressione dei tasti CTRL+C
signal.signal(signal.SIGINT, signal_handler)

# loop del server
try:
    while True:
        server.serve_forever()
except KeyboardInterrupt:
    pass
server.server_close()