import codecs
import threading


def get_error_signup_yet():
    """
    Get an error page to show that the mail used to sign up is registered yet.

    Returns
    -------
    Page HTML as string

    """
    log = codecs.open("./meta_html/meta_log_sign.html", "r", "utf-8").read()
    err = '<p style="color:red">Utente già registrato!</p>'
    return log.format(id_section="signup",title="REGISTRAZIONE",type="signup",button="Registra",page="./",ref="Sei già registrato? Clicca qui!",error=err)

def get_error_login():
    """
    Get an error page to show that authentication doesn't end successfuly
    
    Returns
    -------
    Page HTML as string

    """
    log = codecs.open("./meta_html/meta_log_sign.html", "r", "utf-8").read()
    err = '<p style="color:red">Email o password non corrette!</p>'
    return log.format(id_section="login",title="LOGIN",type="login",button="Accedi",page="./sign_up.html",ref="Non sei registrato? Clicca qui!",error=err)

def create_page(page,address,port_number):
    """
    Build an existing html file with the name of the page passed. In a folder
    "meta_html" a file "meta_{page}.html" has to be present
    ----------
    page : name of the page to build

    Returns
    -------
    None.

    """
    with open(f'./{page}.html',"w",encoding="utf-8") as file_:
        # Le pagine per il login e l'iscrizioni hanno una struttura diversa rispetto
        # a quelle del normale sito
        if page == "index":
            log = codecs.open("./meta_html/meta_log_sign.html", "r", "utf-8").read()
            file_.write(log.format(id_section="login",title="LOGIN",type="login",button="Accedi",page="./sign_up.html",ref="Non sei registrato? Clicca qui!",error=""))
        elif page == "sign_up":
            log = codecs.open("./meta_html/meta_log_sign.html", "r", "utf-8").read()
            file_.write(log.format(id_section="signup",title="REGISTRAZIONE",type="signup",button="Registra",page="./",ref="Sei già registrato? Clicca qui!",error=""))
        else:
            scrip = codecs.open("./meta_html/script_burger.html","r",encoding="utf-8").read()
            tn = codecs.open("./meta_html/topnav.html","r",encoding="utf-8").read().format(ip=address,port=port_number)
            conten = codecs.open(f'./meta_html/meta_{page}.html',"r",encoding="utf-8").read()
            styl = codecs.open("./meta_html/style.html","r",encoding="utf-8").read()
            schema = codecs.open("./meta_html/meta_schema.html","r",encoding="utf-8").read()
            
            # Sfrutto dei template html per poi costruire la pagina effettiva
            cc = schema.format(style=styl,topnav=tn,content_body=conten,script=scrip)
            file_.write(cc)
    

def refresh_pages(waiting,host,port):
    """
    Refresh pages of website. Host and port are necessary to create correct links
    Parameters
    ----------
    waiting : threadin.Event object to coordinate with other thread
    host : ip adress of server
    port : port of application

    Returns
    -------
    None.

    """

    while not waiting.is_set():
        print("Aggiornamento pagine...")
        create_page("index",host,port)
        create_page("sign_up",host,port)
        create_page("contatti",host,port)
        create_page("avvisi",host,port)
        create_page("homepage",host,port)
        create_page("tac",host,port)
        create_page("risonanza_magnetica",host,port)
        create_page("eeg",host,port)
        waiting.wait(timeout=10)

def launch_thread_refresh(waiting_refresh,host,port):
    """
    Launch thread it checks last interact time of logged users
    Parameters
    ----------
    logged_user : dictionary of logged users

    Returns
    -------
    None.

    """
    print("Launching refresh thread")
    check_thread = threading.Thread(target=refresh_pages,args=(waiting_refresh,host,port))
    check_thread.daemon = True
    check_thread.start()
